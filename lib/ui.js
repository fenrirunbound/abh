var path = require('path');

module.exports = {
    main: {
        file: path.resolve(__dirname, '../ui/index.html')
    },
    scripts: {
        directory: {
            path: path.resolve(__dirname, '../ui/scripts')
        }
    }
};