var register,
    router = require('./router');

register = function (server, options, next) {
    router(server, next);
}

register.attributes = {
    pkg: require('./package.json')
};

module.exports = {
    register: register
};