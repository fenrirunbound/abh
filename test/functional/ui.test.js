var expect = require('chai').expect,
    hapi = require('hapi');

describe('Main functional test', function () {
    var main,
        server;

    beforeEach(function (done) {
        main = require('../../index');
        server = new hapi.Server();
        server.connection({port: 3000});
        server.register({
            register: main,
            options: {}
        }, done);
    });

    afterEach(function (done) {
        server.stop(function () {
            server = null;
            done();
        });
    });

    it('serves the main game page', function (done) {
        server.inject({
            method: 'GET',
            url: '/'
        }, function (response) {
            expect(response.statusCode).to.equal(200);
            expect(response.headers['content-type']).to.equal('text/html; charset=utf-8');
            done();
        });
    });

    it('serves the page scripts', function (done) {
        server.inject({
            method: 'GET',
            url: '/scripts/main.js'
        }, function (response) {
            expect(response.statusCode).to.equal(200);
            expect(response.headers['content-type']).to.equal('application/javascript; charset=utf-8');
            done();
        });
    });
});