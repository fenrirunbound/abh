var expect = require('chai').expect,
    hapi = require('hapi');

describe('Main functional test', function () {
    var main,
        server;

    beforeEach(function (done) {
        main = require('../../index');
        server = new hapi.Server();
        server.connection({port: 3000});
        server.register({
            register: main,
            options: {}
        }, done);
    });

    afterEach(function (done) {
        server.stop(function () {
            server = null;
            done();
        });
    });

    it('has a functional api', function (done) {
        server.inject({
            method: 'GET',
            url: '/api/status'
        }, function (response) {
            var data;
            expect(response.statusCode).to.equal(200);
            data = JSON.parse(response.payload);

            expect(data).to.deep.equal({
                status: 'OK'
            });
            done();
        });
    });
});