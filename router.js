var inert = require('inert'),
    statusRoute = require('./lib/status'),
    uiRoute = require('./lib/ui');

function preRoute(server, callback) {
    // Prefix all routes
    if (server.realm.modifiers.route.prefix) {
        server.realm.modifiers.route.prefix += '/abh';
    }

    server.register(inert, callback);
}

function setupRoutes(server, callback) {
    server.route({
        method: 'GET',
        path: '/api/status',
        handler: statusRoute
    });

    server.route({
        method: 'GET',
        path: '/scripts/{filename*}',
        handler: uiRoute.scripts
    });

    server.route({
        method: 'GET',
        path: '/{param*}',
        handler: uiRoute.main
    });

    callback();
}

module.exports = function (server, callback) {
    preRoute(server, function () {
        setupRoutes(server, callback);
    });
};
